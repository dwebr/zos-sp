#pragma once
#include <string>

const int CLUSTER_SIZE = 4096;
const int ITEM_NAME_LENGTH = 12;
const int SIGNATURE_LENGTH = 10;
const int VOLUME_DESCRIPTOR_LENGTH = 20;
const double CLUSTER_TO_INODE_RATIO = 10;

const std::string SIGNATURE = "dwebr";
const std::string VOLUME_DESCRIPTOR = "SP KIV/ZOS";

const int INVALID_ID = -1;
