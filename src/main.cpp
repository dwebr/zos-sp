#include <iostream>
#include <streambuf>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>
#include <array>
#include <sstream>
#include <numeric>
#include <cstring>
#include <bits/stdc++.h>

#include "constants.h"
#include "structs.h"
#include "utils.h"

#undef min

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        std::cout << "Wrong arguments" << std::endl;
        return 1;
    }
    std::string FSName = argv[1];
    std::string promptText = FSName;
    promptText += ">";
    std::string command;
    std::fstream fileSystemImage;

    Superblock sb;
    int currentDirectoryINodeId = 0;
    std::vector<u_char> bitmapInode;
    std::vector<u_char> bitmapCluster;
    std::vector<PseudoInode> inodes;

    int bitmapInodelength;
    int bitmapClusterlength;

    int maxItemsInCluster = CLUSTER_SIZE / sizeof(DirectoryItem);
    size_t maxLinksInCluster = CLUSTER_SIZE / sizeof(size_t);

    /**
     * Returns vector(length pd count) containing id on free clusters      
    */
    auto getFreeClusters = [&sb, &bitmapCluster](int count) -> std::vector<size_t> {
        std::vector<size_t> freeClusters;
        for (size_t i = 0; i < sb.clusterCount; i++)
        {
            if ((bitmapCluster[i] & 0b1) != 1)
            {
                freeClusters.push_back(i);
            }
            if (freeClusters.size() == count)
            {
                break;
            }
        }
        return freeClusters;
    };

    /**
     * Set direct and indirect references to inode data clusters      
    */
    auto setINodeClusters = [&fileSystemImage, &sb, &maxLinksInCluster, &getFreeClusters, &bitmapCluster](PseudoInode &inode, std::vector<size_t> clusters) -> int {
        //number of indirect clusters needed
        int indirectClustersCount = 0;
        if (clusters.size() > 5)
        {
            indirectClustersCount++;
        }
        if (clusters.size() > (5 + maxLinksInCluster))
        {
            indirectClustersCount++;
            indirectClustersCount += std::ceil((clusters.size() - 5 - maxLinksInCluster) / (double)maxLinksInCluster);
        }

        auto indirectClusters = getFreeClusters(indirectClustersCount);
        if (indirectClusters.size() < indirectClustersCount)
        {
            return 1;
        }

        //set indirect clusters in bitmap
        for (int indirectCluster : indirectClusters)
        {
            bitmapCluster[indirectCluster] = 1;
        }

        int count = clusters.size();
        //direct
        if (count >= 1)
        {
            inode.direct1 = clusters[0];
        }
        if (count >= 2)
        {
            inode.direct2 = clusters[1];
        }
        if (count >= 3)
        {
            inode.direct3 = clusters[2];
        }
        if (count >= 4)
        {
            inode.direct4 = clusters[3];
        }
        if (count >= 5)
        {
            inode.direct5 = clusters[4];
        }
        if (count > 5)
        {
            //indirect 1
            inode.indirect1 = indirectClusters[0];
            fileSystemImage.seekg(sb.clustersStartAddress + indirectClusters[0] * CLUSTER_SIZE, std::ios::beg);
            for (int i = 0; i < maxLinksInCluster; i++)
            {
                int clusterId = 5 + i;
                if (clusterId >= clusters.size())
                {
                    return 0;
                }
                //direct
                fileSystemImage.write(reinterpret_cast<const char *>(&clusters[5 + i]), sizeof(size_t));
            }
        }
        if (count > (5 + maxLinksInCluster))
        {
            //indirect 2
            inode.indirect2 = indirectClusters[1];
            for (int i1 = 0; i1 < maxLinksInCluster; i1++)
            {
                fileSystemImage.seekg(sb.clustersStartAddress + indirectClusters[1] * CLUSTER_SIZE + i1 * sizeof(size_t), std::ios::beg);
                //indirect 1
                fileSystemImage.write(reinterpret_cast<const char *>(&indirectClusters[2 + i1]), sizeof(size_t));
                fileSystemImage.seekg(sb.clustersStartAddress + indirectClusters[2 + i1] * CLUSTER_SIZE, std::ios::beg);
                for (int i2 = 0; i2 < maxLinksInCluster; i2++)
                {
                    int clusterId = 5 + maxLinksInCluster + i1 * maxLinksInCluster + i2;
                    if (clusterId >= clusters.size())
                    {
                        return 0;
                    }
                    //direct
                    fileSystemImage.write(reinterpret_cast<const char *>(&clusters[clusterId]), sizeof(size_t));
                }
            }
        }
        return 0;
    };

    /**
     * Return lowest free inodeId      
    */
    auto getFreeInodeId = [&inodes, &sb, &bitmapInode]() -> int {
        int freeInodeId = INVALID_ID;
        for (int i = 0; i < sb.inodeCount; i++)
        {
            if ((bitmapInode[i] & 0b1) != 1)
            {
                freeInodeId = i;
                break;
            }
        }
        return freeInodeId;
    };

    /**
     * Returns vector with all clustersIDs that inode uses to store data    
    */
    auto getFileClusters = [&inodes, &maxLinksInCluster, &fileSystemImage, &sb](int inodeId, bool onlyDataClasters = true) -> std::vector<int> {
        PseudoInode fileNode = inodes[inodeId];
        int clusterCount = std::ceil(fileNode.fileSize / (double)CLUSTER_SIZE);
        std::vector<int> clusters;
        int clustersLoaded = 0;

        //direct
        if (clusterCount >= 1)
        {
            clusters.push_back(fileNode.direct1);
        }
        if (clusterCount >= 2)
        {
            clusters.push_back(fileNode.direct2);
        }
        if (clusterCount >= 3)
        {
            clusters.push_back(fileNode.direct3);
        }
        if (clusterCount >= 4)
        {
            clusters.push_back(fileNode.direct4);
        }
        if (clusterCount >= 5)
        {
            clusters.push_back(fileNode.direct5);
        }
        //indirect 1
        if (clusterCount > 5)
        {
            clustersLoaded = 5;
            if (!onlyDataClasters)
            {
                clusters.push_back(fileNode.indirect1);
            }
            fileSystemImage.seekg(sb.clustersStartAddress + fileNode.indirect1 * CLUSTER_SIZE, std::ios::beg);
            size_t clusterId;
            for (int i = 0; i < maxLinksInCluster; i++)
            {
                fileSystemImage.read(reinterpret_cast<char *>(&clusterId), sizeof(clusterId));
                clusters.push_back(clusterId);
                clustersLoaded++;
                if (clustersLoaded >= clusterCount)
                {
                    return clusters;
                }
            }
        }
        //indirect 2
        if (clusterCount > (5 + maxLinksInCluster))
        {
            if (!onlyDataClasters)
            {
                clusters.push_back(fileNode.indirect2);
            }
            size_t indirectClusterId;
            size_t clusterId;
            for (int i = 0; i < maxLinksInCluster; i++)
            {
                fileSystemImage.seekg(sb.clustersStartAddress + fileNode.indirect2 * CLUSTER_SIZE + i * sizeof(size_t), std::ios::beg);
                fileSystemImage.read(reinterpret_cast<char *>(&indirectClusterId), sizeof(indirectClusterId));
                if (!onlyDataClasters)
                {
                    clusters.push_back(indirectClusterId);
                }
                fileSystemImage.seekg(sb.clustersStartAddress + indirectClusterId * CLUSTER_SIZE, std::ios::beg);
                for (int i2 = 0; i2 < maxLinksInCluster; i2++)
                {
                    fileSystemImage.read(reinterpret_cast<char *>(&clusterId), sizeof(clusterId));
                    clusters.push_back(clusterId);
                    clustersLoaded++;
                    if (clustersLoaded >= clusterCount)
                    {
                        return clusters;
                    }
                }
            }
        }
        return clusters;
    };

    /**
     * Returns lowest free item in directory cluster  
    */
    auto getFreeItemIdInDirCluster = [&sb, &fileSystemImage, &maxItemsInCluster](int clusterId) -> int {
        int itemId = INVALID_ID;
        fileSystemImage.seekg(sb.clustersStartAddress + clusterId * CLUSTER_SIZE + 2 * sizeof(DirectoryItem), std::ios::beg);
        for (int i = 2; i < maxItemsInCluster; i++)
        {
            DirectoryItem item;
            fileSystemImage.read(reinterpret_cast<char *>(&item), sizeof(item));
            if (item.inode == INVALID_ID)
            {
                itemId = i;
                break;
            }
        }
        return itemId;
    };

    /**
     * Returns all directory items
    */
    auto getItemsInDirectory = [&inodes, &sb, &fileSystemImage, &maxItemsInCluster](int directoryNodeId) -> std::vector<DirectoryItem> {
        std::vector<DirectoryItem> items;
        PseudoInode directoryNode = inodes[directoryNodeId];
        fileSystemImage.seekg(sb.clustersStartAddress + directoryNode.direct1 * CLUSTER_SIZE, std::ios::beg);
        for (int i = 0; i < maxItemsInCluster; i++)
        {
            DirectoryItem item;
            fileSystemImage.read(reinterpret_cast<char *>(&item), sizeof(item));
            if (item.inode != INVALID_ID)
            {
                items.push_back(item);
            }
        }
        return items;
    };

    /**
     * Deletes directory item
    */
    auto removeDirectoryItem = [&sb, &fileSystemImage, &maxItemsInCluster](int clusterId, std::string name) {
        fileSystemImage.seekg(sb.clustersStartAddress + clusterId * CLUSTER_SIZE, std::ios::beg);
        for (int i = 0; i < maxItemsInCluster; i++)
        {
            DirectoryItem item;
            fileSystemImage.read(reinterpret_cast<char *>(&item), sizeof(item));
            if (item.itemName == name)
            {
                DirectoryItem invalidItem = {INVALID_ID, ""};
                fileSystemImage.seekg(sb.clustersStartAddress + clusterId * CLUSTER_SIZE + i * sizeof(DirectoryItem), std::ios::beg);
                fileSystemImage.write(reinterpret_cast<const char *>(&invalidItem), sizeof(invalidItem));
            }
        }
    };

    /**
     * Returns nodeId defined by pathVector
    */
    auto getInodeId = [&currentDirectoryINodeId, &getItemsInDirectory, &inodes](std::vector<std::string> pathVector) -> int {
        if (pathVector.size() == 0)
        {
            return currentDirectoryINodeId;
        }

        int startNodeId;
        if (pathVector[0] == "")
        {
            if (pathVector.size() == 1)
            {
                return 0;
            }
            pathVector.erase(pathVector.begin());
            startNodeId = 0;
        }
        else
        {
            startNodeId = currentDirectoryINodeId;
        }

        auto items = getItemsInDirectory(startNodeId);
        DirectoryItem pathDir;
        for (std::string dir : pathVector)
        {
            bool exists = false;
            for (auto item : items)
            {
                if (item.itemName == dir)
                {
                    if (!inodes[item.inode].isDirectory)
                    {
                        if (dir == pathVector.back())
                        {
                            return item.inode;
                        }
                    }
                    pathDir = item;
                    exists = true;
                    break;
                }
            }

            if (!exists)
            {
                return INVALID_ID;
            }

            items = getItemsInDirectory(pathDir.inode);
        }

        return pathDir.inode;
    };

    /**
     * Saves header
    */
    auto saveHeader = [&fileSystemImage, &sb]() {
        fileSystemImage.seekg(0, std::ios::beg);
        fileSystemImage.write(reinterpret_cast<const char *>(&sb), sizeof(sb));
    };

    /**
     * Saves bitmap of clusters
    */
    auto saveClusterBitmap = [&fileSystemImage, &bitmapCluster, &sb, &bitmapClusterlength]() {
        fileSystemImage.seekg(sb.bitmapClustersStart, std::ios::beg);
        for (int i = 0; i < bitmapClusterlength; i++)
        {
            std::bitset<8> byte;
            for (int b = 0; b < 8; b++)
            {
                byte.set(7 - b, bitmapCluster[i * 8 + b] & 0b1);
            }
            fileSystemImage.write(reinterpret_cast<const char *>(&byte), 1);
        }
    };

    /**
     * Saves bitmap of inodes
    */
    auto saveInodeBitmap = [&fileSystemImage, &bitmapInode, &sb, &bitmapInodelength]() {
        fileSystemImage.seekg(sb.bitmapInodesStart, std::ios::beg);
        for (int i = 0; i < bitmapInodelength; i++)
        {
            std::bitset<8> byte;
            for (int b = 0; b < 8; b++)
            {
                byte.set(7 - b, bitmapInode[i * 8 + b] & 0b1);
            }
            fileSystemImage.write(reinterpret_cast<const char *>(&byte), 1);
        }
    };

    /**
     * Saves inodes
    */
    auto saveInodes = [&fileSystemImage, &inodes, &sb, &bitmapInode]() {
        for (int i = 0; i < sb.inodeCount; i++)
        {
            if ((bitmapInode[i] & 0b1) == 1)
            {
                PseudoInode node = inodes[i];
                fileSystemImage.seekg(sb.inodeStart + node.nodeId * sizeof(PseudoInode), std::ios::beg);
                fileSystemImage.write(reinterpret_cast<const char *>(&node), sizeof(node));
            }
        }
    };

    /**
     * handles command
    */
    std::function<int(std::string)> handleInput;
    handleInput = [&](std::string command) -> int {
        /**
         * empty command
        */
        if (command == "")
        {
            return 0;
        }
        auto parameters = explode(command);
        std::string commandType = parameters[0];
        // ================================================================================
        // command exit
        // ================================================================================
        if (commandType == "exit")
        {
            std::cout << "Exit successful" << std::endl;
            return 1;
        }
        // ================================================================================
        // command format file system
        // ================================================================================
        else if (commandType == "format")
        {
            size_t size = getBytes(parameters[1]);
            if (size <= 0)
            {
                std::cout << "INVALID SIZE" << std::endl;
                return 0;
            }

            //remove filesystem file if already exists
            if (fileSystemImage.is_open())
            {
                remove(FSName.c_str());
                fileSystemImage = std::fstream();
            }

            //open file
            fileSystemImage.open(FSName, std::ios::binary | std::ios::trunc | std::ios::in | std::ios::out);

            //check if open
            if (!fileSystemImage.is_open())
            {
                std::cout << "CANNOT CREATE FILE" << std::endl;
                return 0;
            }

            //fill with 0
            for (int i = 0; i < size; i++)
                fileSystemImage.write("", 1);

            //calculate parameters
            int freeSize = size - sizeof(Superblock);
            int clusterCount = freeSize / (((1 / 8) + sizeof(PseudoInode)) / CLUSTER_TO_INODE_RATIO + (1 / 8) + CLUSTER_SIZE);
            int inodeCount = clusterCount / CLUSTER_TO_INODE_RATIO;
            bitmapClusterlength = (clusterCount % 8) > 0 ? (clusterCount / 8) + 1 : (clusterCount / 8);
            bitmapInodelength = (inodeCount % 8) > 0 ? (inodeCount / 8) + 1 : (inodeCount / 8);

            //fill superblock
            strcpy(sb.signature, SIGNATURE.c_str());
            strcpy(sb.volumeDescriptor, VOLUME_DESCRIPTOR.c_str());
            sb.diskSize = size;
            sb.inodeCount = inodeCount;
            sb.clusterSize = CLUSTER_SIZE;
            sb.clusterCount = clusterCount;
            sb.bitmapInodesStart = sizeof(Superblock);
            sb.bitmapClustersStart = sizeof(Superblock) + bitmapInodelength;
            sb.inodeStart = sizeof(Superblock) + bitmapInodelength + bitmapClusterlength;
            sb.clustersStartAddress = sizeof(Superblock) + bitmapClusterlength + bitmapInodelength + inodeCount * sizeof(PseudoInode);

            saveHeader();

            //save inodes bitmap
            fileSystemImage.seekg(sb.bitmapInodesStart, std::ios::beg);
            std::bitset<8> firstInodesBitset("100000000");
            fileSystemImage.write(reinterpret_cast<const char *>(&firstInodesBitset), 1);
            bitmapInode.resize(bitmapInodelength * 8);
            std::fill(bitmapInode.begin(), bitmapInode.end(), 0);
            bitmapInode[0] = 1;

            //save cluster bitmap
            fileSystemImage.seekg(sb.bitmapClustersStart, std::ios::beg);
            std::bitset<8> firstClusterBitset("100000000");
            fileSystemImage.write(reinterpret_cast<const char *>(&firstClusterBitset), 1);
            bitmapCluster.resize(bitmapClusterlength * 8);
            std::fill(bitmapCluster.begin(), bitmapCluster.end(), 0);
            bitmapCluster[0] = 1;

            //save root inode
            fileSystemImage.seekg(sb.inodeStart, std::ios::beg);
            PseudoInode rootDirInode = {0, true, 1, CLUSTER_SIZE, 0};
            fileSystemImage.write(reinterpret_cast<const char *>(&rootDirInode), sizeof(PseudoInode));
            inodes.resize(sb.inodeCount);
            inodes[0] = rootDirInode;

            //create root directory cluster
            fileSystemImage.seekg(sb.clustersStartAddress, std::ios::beg);
            DirectoryItem selfReference = {0, "."};
            DirectoryItem parentReference = {0, ".."};
            fileSystemImage.write(reinterpret_cast<const char *>(&selfReference), sizeof(selfReference));
            fileSystemImage.write(reinterpret_cast<const char *>(&parentReference), sizeof(parentReference));
            for (int i = 2; i < maxItemsInCluster; i++)
            {
                //fill rest with invalid items
                DirectoryItem invalidItem = {INVALID_ID, ""};
                fileSystemImage.write(reinterpret_cast<const char *>(&invalidItem), sizeof(invalidItem));
            }

            printSuperblock(sb);

            std::cout << "OK" << std::endl;
        }
        // ================================================================================
        // command load and execite file with commands
        // ================================================================================
        else if (commandType == "load")
        {
            std::ifstream infile(parameters[1]);
            if (!infile.is_open())
            {
                std::cout << "FILE NOT FOUND" << std::endl;
                return 0;
            }
            //handle every line of file
            std::string line;
            while (std::getline(infile, line))
            {
                std::cout << promptText << line << std::endl;
                handleInput(line);
            }
            return 0;
        }
        // ================================================================================
        // If fs is not formated exit if structure
        // ================================================================================
        else if (!fileSystemImage.is_open())
        {
            std::cout << "File system is not formated!" << std::endl;
        }
        // ================================================================================
        // command copy file
        // ================================================================================
        else if (commandType == "cp")
        {
            if (parameters.size() <= 2)
            {
                std::cout << "Missing argument" << std::endl;
                return 0;
            }
            //get source node
            auto pathVectorsSource = explode(parameters[1], '/');
            std::string sourceName = pathVectorsSource.back();
            int nodeIdSource = getInodeId(pathVectorsSource);
            if (nodeIdSource == INVALID_ID)
            {
                std::cout << "FILE NOT FOUND" << std::endl;
                return 0;
            }
            PseudoInode &nodeSource = inodes[nodeIdSource];
            if (nodeSource.isDirectory)
            {
                std::cout << "FILE NOT FOUND" << std::endl;
                return 0;
            }
            // get destination node
            auto pathVectorsDestination = explode(parameters[2], '/');
            std::string newName = pathVectorsDestination.back();
            pathVectorsDestination.pop_back();
            int nodeIdDestination = getInodeId(pathVectorsDestination);
            if (nodeIdDestination == INVALID_ID)
            {
                std::cout << "PATH NOT FOUND" << std::endl;
                return 0;
            }
            PseudoInode nodeDestination = inodes[nodeIdDestination];

            //get id of new item
            int itemId = getFreeItemIdInDirCluster(nodeDestination.direct1);
            if (itemId == INVALID_ID)
            {
                std::cout << "DIRECTORY IS FULL" << std::endl;
                return 0;
            }

            //get id of new inode
            int freeInodeId = getFreeInodeId();
            if (freeInodeId == INVALID_ID)
            {
                std::cout << "Free inode not found" << std::endl;
                return 0;
            }

            // calclate number of required clusters
            int requiredClusters = std::ceil(nodeSource.fileSize / (double)CLUSTER_SIZE);

            // get free clusters ids
            auto freeClusters = getFreeClusters(requiredClusters);
            if (freeClusters.size() < requiredClusters)
            {
                std::cout << "Not enough free clusters" << std::endl;
                return 0;
            }

            //edit inode bitmap
            bitmapInode[freeInodeId] = 1;

            //copy clusters of existing file into free clusters
            auto fileClusters = getFileClusters(nodeIdSource);
            u_char fileBytes[CLUSTER_SIZE];
            for (int i = 0; i < fileClusters.size(); i++)
            {
                int freeCluster = freeClusters[i];
                int fileCluster = fileClusters[i];

                bitmapCluster[freeCluster] = 1;

                fileSystemImage.seekg(sb.clustersStartAddress + fileCluster * CLUSTER_SIZE, std::ios::beg);
                fileSystemImage.read(reinterpret_cast<char *>(&fileBytes), CLUSTER_SIZE);
                fileSystemImage.seekg(sb.clustersStartAddress + freeCluster * CLUSTER_SIZE, std::ios::beg);
                fileSystemImage.write(reinterpret_cast<const char *>(&fileBytes), CLUSTER_SIZE);
            }

            //create new inode
            PseudoInode newFileNode = {
                freeInodeId,
                false,
                1,
                nodeSource.fileSize};
            int result = setINodeClusters(newFileNode, freeClusters);
            if (result > 0)
            {
                std::cout << "Not enough free clusters" << std::endl;
                return 0;
            }
            inodes[freeInodeId] = newFileNode;

            //create new directory
            DirectoryItem newDirectory = {freeInodeId};
            strcpy(newDirectory.itemName, newName.c_str());
            int offsetNewDirectoryItem = sb.clustersStartAddress + nodeDestination.direct1 * CLUSTER_SIZE + itemId * sizeof(DirectoryItem);
            fileSystemImage.seekg(offsetNewDirectoryItem, std::ios::beg);
            fileSystemImage.write(reinterpret_cast<const char *>(&newDirectory), sizeof(newDirectory));

            //save data into filesystem image
            saveInodeBitmap();
            saveClusterBitmap();
            saveInodes();
            std::cout << "OK" << std::endl;
        }
        // ================================================================================
        // command move file
        // ================================================================================
        else if (commandType == "mv")
        {
            if (parameters.size() <= 2)
            {
                std::cout << "Missing argument" << std::endl;
                return 0;
            }
            // load source inode
            auto pathVectorsSource = explode(parameters[1], '/');
            int nodeIdSource = getInodeId(pathVectorsSource);
            if (nodeIdSource == INVALID_ID)
            {
                std::cout << "FILE NOT FOUND" << std::endl;
                return 0;
            }
            PseudoInode &nodeSource = inodes[nodeIdSource];
            if (nodeSource.isDirectory)
            {
                std::cout << "FILE NOT FOUND" << std::endl;
                return 0;
            }

            //load source directory inode
            std::string oldname = pathVectorsSource.back();
            pathVectorsSource.pop_back();
            int nodeIdSourceDir = getInodeId(pathVectorsSource);
            if (nodeIdSourceDir == INVALID_ID)
            {
                std::cout << "FILE NOT FOUND" << std::endl;
                return 0;
            }
            PseudoInode &nodeSourceDir = inodes[nodeIdSourceDir];

            //load destination directory inode
            auto pathVectorsDestination = explode(parameters[2], '/');
            std::string newName = pathVectorsDestination.back();
            pathVectorsDestination.pop_back();
            int nodeIdDestination = getInodeId(pathVectorsDestination);
            if (nodeIdDestination == INVALID_ID)
            {
                std::cout << "PATH NOT FOUND" << std::endl;
                return 0;
            }
            PseudoInode nodeDestination = inodes[nodeIdDestination];

            //get free item id in destination folder
            int itemId = getFreeItemIdInDirCluster(nodeDestination.direct1);
            if (itemId == INVALID_ID)
            {
                std::cout << "DIRECTORY IS FULL" << std::endl;
                return 0;
            }

            //remove item in source folder
            removeDirectoryItem(nodeSourceDir.direct1, oldname);

            //save new directory
            DirectoryItem newDirectory = {nodeIdSource};
            strcpy(newDirectory.itemName, newName.c_str());
            int offsetNewDirectoryItem = sb.clustersStartAddress + nodeDestination.direct1 * CLUSTER_SIZE + itemId * sizeof(DirectoryItem);
            fileSystemImage.seekg(offsetNewDirectoryItem, std::ios::beg);
            fileSystemImage.write(reinterpret_cast<const char *>(&newDirectory), sizeof(newDirectory));

            std::cout << "OK" << std::endl;
        }
        // ================================================================================
        // command remove file
        // ================================================================================
        else if (commandType == "rm")
        {
            if (parameters.size() <= 1)
            {
                std::cout << "Missing argument" << std::endl;
                return 0;
            }

            //load file inode
            std::string path = parameters[1];
            auto pathVector = explode(path, '/');
            int fileNodeId = getInodeId(pathVector);

            if (fileNodeId == INVALID_ID)
            {
                std::cout << "FILE NOT FOUND" << std::endl;
                return 0;
            }
            PseudoInode &fileNode = inodes[fileNodeId];
            if (fileNode.isDirectory)
            {
                std::cout << "FILE NOT FOUND" << std::endl;
                return 0;
            }

            //load paren directory id
            std::string name = pathVector.back();
            pathVector.pop_back();
            int parentDirNodeId = getInodeId(pathVector);
            PseudoInode parentDir = inodes[parentDirNodeId];
            removeDirectoryItem(parentDir.direct1, name);

            //check reference, mark clusters and inode as free if file has only 1 reference
            if (fileNode.references <= 1)
            {
                bitmapInode[fileNodeId] = 0;
                auto clusters = getFileClusters(fileNodeId, false);
                for (int clusterId : clusters)
                {
                    bitmapCluster[clusterId] = 0;
                }
                saveInodeBitmap();
                saveClusterBitmap();
            }
            else
            {
                fileNode.references--;
                saveInodes();
            }

            std::cout << "OK" << std::endl;
        }
        // ================================================================================
        // command create directory
        // ================================================================================
        else if (commandType == "mkdir")
        {
            if (parameters.size() <= 1)
            {
                std::cout << "Missing argument" << std::endl;
                return 0;
            }
            std::string path = parameters[1];
            auto pathVector = explode(path, '/');
            std::string name = pathVector.back();
            pathVector.pop_back();

            //free inode id
            int newInodeId = getFreeInodeId();
            if (newInodeId == INVALID_ID)
            {
                std::cout << "Free inode not found" << std::endl;
                return 0;
            }

            //free cluster id
            auto clusters = getFreeClusters(1);
            if (clusters.size() == 0)
            {
                std::cout << "Free cluster not found" << std::endl;
                return 0;
            }
            size_t newClusterId = clusters[0];

            //load parent dir inode
            int parentDirId = getInodeId(pathVector);
            if (parentDirId == INVALID_ID)
            {
                std::cout << "PATH NOT FOUND" << std::endl;
                return 0;
            }
            PseudoInode &parentDir = inodes[parentDirId];

            //check if item exists
            bool nameExists = false;
            auto items = getItemsInDirectory(parentDirId);
            for (auto item : items)
            {
                if (item.itemName == name)
                {
                    nameExists = true;
                    break;
                }
            }
            if (nameExists)
            {
                std::cout << "EXIST" << std::endl;
                return 0;
            }

            //get free item id in parent cluster
            int itemId = getFreeItemIdInDirCluster(parentDir.direct1);
            if (itemId == INVALID_ID)
            {
                std::cout << "DIRECTORY IS FULL" << std::endl;
                return 0;
            }

            //edit bitmaps
            bitmapInode[newInodeId] = 1;
            bitmapCluster[newClusterId] = 1;

            PseudoInode newDirectoryInode = {
                newInodeId,
                true,
                1,
                CLUSTER_SIZE,
                newClusterId};

            DirectoryItem newDirectory = {newInodeId};
            strcpy(newDirectory.itemName, name.c_str());

            //save directory item in parent direcotry
            int offsetNewDirectoryItem = sb.clustersStartAddress + parentDir.direct1 * CLUSTER_SIZE + itemId * sizeof(DirectoryItem);
            fileSystemImage.seekg(offsetNewDirectoryItem, std::ios::beg);
            fileSystemImage.write(reinterpret_cast<const char *>(&newDirectory), sizeof(newDirectory));

            //create cluster with directory items for new directory
            int offsetNewDirCluster = sb.clustersStartAddress + newClusterId * CLUSTER_SIZE;
            fileSystemImage.seekg(offsetNewDirCluster, std::ios::beg);
            DirectoryItem selfReference = {newInodeId, "."};
            DirectoryItem parentReference = {parentDirId, ".."};
            fileSystemImage.write(reinterpret_cast<const char *>(&selfReference), sizeof(selfReference));
            fileSystemImage.write(reinterpret_cast<const char *>(&parentReference), sizeof(parentReference));
            for (int i = 2; i < maxItemsInCluster; i++)
            {
                DirectoryItem invalidItem = {INVALID_ID, ""};
                fileSystemImage.write(reinterpret_cast<const char *>(&invalidItem), sizeof(invalidItem));
            }

            inodes[newInodeId] = newDirectoryInode;

            //write data to fs image
            saveInodeBitmap();
            saveClusterBitmap();
            saveInodes();

            std::cout << "OK" << std::endl;
        }
        // ================================================================================
        // command delete directory
        // ================================================================================
        else if (commandType == "rmdir")
        {
            if (parameters.size() <= 1)
            {
                std::cout << "Missing argument" << std::endl;
                return 0;
            }

            //get directory inode id
            std::string path = parameters[1];
            auto pathVector = explode(path, '/');
            int directoryNodeId = getInodeId(pathVector);
            if (directoryNodeId == INVALID_ID)
            {
                std::cout << "FILE NOT FOUND" << std::endl;
                return 0;
            }

            //check if directory is empty
            if (getItemsInDirectory(directoryNodeId).size() > 2)
            {
                std::cout << "NOT EMPTY" << std::endl;
                return 0;
            }

            //check if it is root directory
            std::string dirName = pathVector.back();
            pathVector.pop_back();
            int parentDirectoryNodeId = getInodeId(pathVector);
            if (parentDirectoryNodeId == directoryNodeId)
            {
                std::cout << "CANNOT REMOVE ROOT DIRECTORY" << std::endl;
                return 0;
            }

            //edit bitmaps
            int directoryClusterId = inodes[directoryNodeId].direct1;
            bitmapInode[directoryNodeId] = 0;
            bitmapCluster[directoryClusterId] = 0;

            //remove direcotry item from paren directory
            int parentDirectoryClusterId = inodes[parentDirectoryNodeId].direct1;
            removeDirectoryItem(parentDirectoryClusterId, dirName);

            //save bitmaps
            saveInodeBitmap();
            saveClusterBitmap();
            std::cout << "OK" << std::endl;
        }
        // ================================================================================
        // command print directory
        // ================================================================================
        else if (commandType == "ls")
        {
            auto items = getItemsInDirectory(currentDirectoryINodeId);
            for (auto item : items)
            {
                PseudoInode node = inodes[item.inode];
                std::string prefix = node.isDirectory ? "+" : "-";
                std::cout << prefix << item.itemName << std::endl;
            }
        }
        // ================================================================================
        // command print file
        // ================================================================================
        else if (commandType == "cat")
        {
            if (parameters.size() <= 1)
            {
                std::cout << "Missing argument" << std::endl;
                return 0;
            }

            //load file inode
            std::string path = parameters[1];
            auto pathVector = explode(path, '/');
            int fileNodeId = getInodeId(pathVector);

            if (fileNodeId == INVALID_ID)
            {
                std::cout << "PATH NOT FOUND" << std::endl;
                return 0;
            }
            PseudoInode fileNode = inodes[fileNodeId];

            if (fileNode.isDirectory)
            {
                std::cout << "PATH NOT FOUND" << std::endl;
                return 0;
            }

            // get file clusters ids
            std::vector<int> clusters = getFileClusters(fileNodeId);
            size_t bytesPrinted = 0;
            u_char fileBytes[CLUSTER_SIZE];
            //read and print every cluster
            for (int clusterId : clusters)
            {
                size_t bytesToRead = (fileNode.fileSize - bytesPrinted) > CLUSTER_SIZE ? CLUSTER_SIZE : (fileNode.fileSize - bytesPrinted);
                fileSystemImage.seekg(sb.clustersStartAddress + clusterId * CLUSTER_SIZE, std::ios::beg);
                fileSystemImage.read(reinterpret_cast<char *>(&fileBytes), bytesToRead);
                for (int i = 0; i < bytesToRead; i++)
                {
                    std::cout << fileBytes[i];
                }
                bytesPrinted += bytesToRead;
            }
            std::cout << std::endl;
        }
        // ================================================================================
        // command change directory
        // ================================================================================
        else if (commandType == "cd")
        {
            if (parameters.size() <= 1)
            {
                std::cout << "Missing argument" << std::endl;
                return 0;
            }
            std::string destination = parameters[1];
            auto destinationVector = explode(destination, '/');
            int destinationNodeId = getInodeId(destinationVector);

            if (destinationNodeId == INVALID_ID)
            {
                std::cout << "PATH NOT FOUND" << std::endl;
            }
            else
            {
                currentDirectoryINodeId = destinationNodeId;
                std::cout << "OK" << std::endl;
            }
        }
        // ================================================================================
        // command print working directory
        // ================================================================================
        else if (commandType == "pwd")
        {
            //vector of current and all parent nodes id
            std::vector<int> inodesPath;
            PseudoInode node = inodes[currentDirectoryINodeId];
            while (true)
            {
                if (node.nodeId == 0)
                {
                    break;
                }
                inodesPath.push_back(node.nodeId);
                node = inodes[getItemsInDirectory(node.nodeId)[1].inode];
            }
            //reverse vector
            std::reverse(inodesPath.begin(), inodesPath.end());

            //store inode name for every inode id
            std::ostringstream path;
            auto items = getItemsInDirectory(0);
            for (int i : inodesPath)
            {
                for (auto item : items)
                {
                    if (item.inode == i)
                    {
                        path << "/" << item.itemName;
                        break;
                    }
                }
                items = getItemsInDirectory(i);
            }
            std::string stringPath = path.str().size() == 0 ? "/" : path.str();
            std::cout << stringPath << std::endl;
        }
        // ================================================================================
        // command print info
        // ================================================================================
        else if (commandType == "info")
        {
            if (parameters.size() <= 1)
            {
                std::cout << "Missing argument" << std::endl;
                return 0;
            }
            //get inode
            auto pathVector = explode(parameters[1], '/');
            int nodeId = getInodeId(pathVector);
            if (nodeId == INVALID_ID)
            {
                std::cout << "FILE NOT FOUND" << std::endl;
                return 0;
            }
            PseudoInode node = inodes[nodeId];

            size_t clusterLinksCount = std::ceil(((double)node.fileSize) / CLUSTER_SIZE);
            std::cout << "NAME – REFERENCES – SIZE – i-node NUMBER – links" << std::endl;
            std::cout << ((pathVector.back() == "") ? "/" : pathVector.back()) << " - " << node.references << " - " << node.fileSize << " - " << node.nodeId << " - ";
            auto clusters = getFileClusters(node.nodeId, false);
            for (int cluster : clusters)
            {
                std::cout << cluster << " ";
            }
            std::cout << std::endl;
        }
        // ================================================================================
        // command copy in
        // ================================================================================
        else if (commandType == "incp")
        {
            if (parameters.size() <= 2)
            {
                std::cout << "Missing argument" << std::endl;
                return 0;
            }
            //get parent directory node id
            auto pathVector = explode(parameters[2], '/');
            std::string newFileName = pathVector.back();
            pathVector.pop_back();
            int nodeId = getInodeId(pathVector);
            if (nodeId == INVALID_ID)
            {
                std::cout << "PATH NOT FOUND" << std::endl;
                return 0;
            }
            PseudoInode parentDirectoryNode = inodes[nodeId];

            if (!parentDirectoryNode.isDirectory)
            {
                std::cout << "PATH NOT FOUND" << std::endl;
                return 0;
            }

            //check if file exists in directory
            auto itemsInDir = getItemsInDirectory(nodeId);
            for (auto item : itemsInDir)
            {
                if (item.itemName == newFileName)
                {
                    std::cout << "EXISTS" << std::endl;
                    return 0;
                }
            }

            //open file to copy in
            std::fstream fileIn;
            fileIn.open(parameters[1], std::ios::binary | std::ios::in | std::ios::out | std::ios::ate);
            size_t size = fileIn.tellg();
            fileIn.seekg(0, std::ios::beg);
            if (!fileIn.is_open())
            {
                std::cout << "FILE NOT FOUND" << std::endl;
                return 0;
            }

            //get free inode id
            int freeInodeId = getFreeInodeId();
            if (freeInodeId == INVALID_ID)
            {
                std::cout << "Free inode not found" << std::endl;
                return 0;
            }

            //get free clusters to save file
            int requiredClusters = std::ceil(size / (double)CLUSTER_SIZE);
            auto freeClusters = getFreeClusters(requiredClusters);
            if (freeClusters.size() < requiredClusters)
            {
                std::cout << "Not enough free clusters" << std::endl;
                return 0;
            }

            // edit inode bitmap
            bitmapInode[freeInodeId] = 1;

            //copy file data into free clusters
            u_char fileBytes[CLUSTER_SIZE];
            size_t bytesWritten = 0;
            for (int i : freeClusters)
            {
                bitmapCluster[i] = 1;

                size_t bytesToRead = (size - bytesWritten) > CLUSTER_SIZE ? CLUSTER_SIZE : (size - bytesWritten);
                fileIn.read(reinterpret_cast<char *>(&fileBytes), bytesToRead);
                fileSystemImage.seekg(sb.clustersStartAddress + i * CLUSTER_SIZE, std::ios::beg);
                fileSystemImage.write(reinterpret_cast<const char *>(&fileBytes), bytesToRead);
                bytesWritten += bytesToRead;
            }

            //create new inode and set clusters
            PseudoInode inode = {
                freeInodeId,
                false,
                1,
                size};
            int result = setINodeClusters(inode, freeClusters);
            if (result > 0)
            {
                std::cout << "Not enough free clusters" << std::endl;
                return 0;
            }
            inodes[freeInodeId] = inode;

            //create new directory item in parent directory
            DirectoryItem newFileItem = {freeInodeId};
            strcpy(newFileItem.itemName, newFileName.c_str());
            int parentDirectoryFreeItem = getFreeItemIdInDirCluster(parentDirectoryNode.direct1);
            int offset = sb.clustersStartAddress + parentDirectoryNode.direct1 * CLUSTER_SIZE + sizeof(DirectoryItem) * parentDirectoryFreeItem;
            fileSystemImage.seekg(offset, std::ios::beg);
            fileSystemImage.write(reinterpret_cast<const char *>(&newFileItem), sizeof(newFileItem));

            //save data into fs image
            saveClusterBitmap();
            saveInodeBitmap();
            saveInodes();

            std::cout << "OK" << std::endl;
        }
        // ================================================================================
        // command copy out
        // ================================================================================
        else if (commandType == "outcp")
        {
            if (parameters.size() <= 2)
            {
                std::cout << "Missing argument" << std::endl;
                return 0;
            }
            //get file inode id
            auto pathVector = explode(parameters[1], '/');
            int nodeId = getInodeId(pathVector);
            if (nodeId == INVALID_ID)
            {
                std::cout << "FILE NOT FOUND" << std::endl;
                return 0;
            }
            PseudoInode fileNode = inodes[nodeId];
            if (fileNode.isDirectory)
            {
                std::cout << "FILE NOT FOUND" << std::endl;
                return 0;
            }

            //get clusters where file has data
            std::vector<int> clusters = getFileClusters(nodeId);

            //create ouput file
            std::fstream outputFile;
            outputFile.open(parameters[2], std::ios::binary | std::ios::trunc | std::ios::in | std::ios::out);
            if (!outputFile.is_open())
            {
                std::cout << "PATH NOT FOUND" << std::endl;
                return 0;
            }

            //write data from cluster into output file
            u_char fileBytes[CLUSTER_SIZE];
            size_t bytesWritten = 0;
            for (int i : clusters)
            {
                size_t bytesToRead = (fileNode.fileSize - bytesWritten) > CLUSTER_SIZE ? CLUSTER_SIZE : (fileNode.fileSize - bytesWritten);
                fileSystemImage.seekg(sb.clustersStartAddress + i * CLUSTER_SIZE, std::ios::beg);
                fileSystemImage.read(reinterpret_cast<char *>(&fileBytes), bytesToRead);
                outputFile.write(reinterpret_cast<const char *>(&fileBytes), bytesToRead);
                bytesWritten += bytesToRead;
            }
            std::cout << "OK" << std::endl;
        }
        // ================================================================================
        // command print status
        // ================================================================================
        else if (commandType == "status")
        {
            std::cout << "Inode bitmap: ";
            for (auto &bit : bitmapInode)
            {
                std::cout << (bit & 0b1);
            }
            std::cout << std::endl;

            std::cout << "Cluster bitmap: ";
            for (auto &bit : bitmapCluster)
            {
                std::cout << (bit & 0b1);
            }
            std::cout << std::endl;

            std::cout << "Inodes: " << std::endl;
            for (int i = 0; i < sb.inodeCount; i++)
            {
                if ((bitmapInode[i] & 0b1) == 1)
                {
                    PseudoInode node = inodes[i];
                    std::cout << "Id: " << node.nodeId << ", references: " << node.references << ", " << ((node.isDirectory == 1) ? "Directory" : "File")
                              << ", size: " << node.fileSize << std::endl;
                }
            }
        }
        // ================================================================================
        // command create hardlink
        // ================================================================================
        else if (commandType == "ln")
        {
            if (parameters.size() <= 2)
            {
                std::cout << "Missing argument" << std::endl;
                return 0;
            }
            //get source file inode
            auto pathVectorsSource = explode(parameters[1], '/');
            int nodeIdSource = getInodeId(pathVectorsSource);
            if (nodeIdSource == INVALID_ID)
            {
                std::cout << "FILE NOT FOUND" << std::endl;
                return 0;
            }
            PseudoInode &nodeSource = inodes[nodeIdSource];
            if (nodeSource.isDirectory)
            {
                std::cout << "FILE NOT FOUND" << std::endl;
                return 0;
            }

            //get destination directory node
            auto pathVectorsDestination = explode(parameters[2], '/');
            std::string hardlinkName = pathVectorsDestination.back();
            pathVectorsDestination.pop_back();
            int nodeIdDestination = getInodeId(pathVectorsDestination);
            if (nodeIdDestination == INVALID_ID)
            {
                std::cout << "PATH NOT FOUND" << std::endl;
                return 0;
            }
            PseudoInode nodeDestination = inodes[nodeIdDestination];
            if (!nodeDestination.isDirectory)
            {
                std::cout << "PATH NOT FOUND" << std::endl;
                return 0;
            }

            //get free item id in destination directory
            int itemId = getFreeItemIdInDirCluster(nodeDestination.direct1);
            if (itemId == INVALID_ID)
            {
                std::cout << "DIRECTORY IS FULL" << std::endl;
                return 0;
            }

            //increase reference count
            nodeSource.references++;

            //create and save directory item
            DirectoryItem hardlinkItem = {nodeSource.nodeId};
            strcpy(hardlinkItem.itemName, hardlinkName.c_str());
            int offsetNewDirectoryItem = sb.clustersStartAddress + nodeDestination.direct1 * CLUSTER_SIZE + itemId * sizeof(DirectoryItem);
            fileSystemImage.seekg(offsetNewDirectoryItem, std::ios::beg);
            fileSystemImage.write(reinterpret_cast<const char *>(&hardlinkItem), sizeof(hardlinkItem));

            //save inodes into fs image
            saveInodes();
            std::cout << "OK" << std::endl;
        }
        // ================================================================================
        // command uknown
        // ================================================================================
        else
        {
            std::cout << "Unkown command" << std::endl;
        }

        return 0;
    };

    //open filesystem if already exists when program starts
    fileSystemImage.open(FSName, std::ios::binary | std::ios::in | std::ios::out);
    if (fileSystemImage.is_open())
    {
        std::cout << "Filesystem image successfully opened." << std::endl;
        //read and print superblock
        fileSystemImage.read(reinterpret_cast<char *>(&sb), sizeof(Superblock));
        printSuperblock(sb);
        //calculate bitmap lengths
        bitmapInodelength = (sb.inodeCount % 8) > 0 ? (sb.inodeCount / 8) + 1 : (sb.inodeCount / 8);
        bitmapClusterlength = (sb.clusterCount % 8) > 0 ? (sb.clusterCount / 8) + 1 : (sb.clusterCount / 8);

        u_char bitmapInodeBytes[bitmapInodelength];
        u_char bitmapClusterBytes[bitmapClusterlength];

        // load save print inode bitmap
        fileSystemImage.seekg(sb.bitmapInodesStart, std::ios::beg);
        fileSystemImage.read(reinterpret_cast<char *>(&bitmapInodeBytes), sizeof(bitmapInodeBytes));
        for (int i = 0; i < bitmapInodelength; i++)
        {
            for (int j = 7; j >= 0; j--)
            {
                bitmapInode.push_back(bitmapInodeBytes[i] >> j);
            }
        }
        std::cout << "Inode bitmap: ";
        for (auto &bit : bitmapInode)
        {
            std::cout << (bit & 0b1);
        }
        std::cout << std::endl;

        // load save print cluster bitmap
        fileSystemImage.seekg(sb.bitmapClustersStart, std::ios::beg);
        fileSystemImage.read(reinterpret_cast<char *>(&bitmapClusterBytes), sizeof(bitmapClusterBytes));
        for (int i = 0; i < bitmapClusterlength; i++)
        {
            for (int j = 7; j >= 0; j--)
            {
                bitmapCluster.push_back(bitmapClusterBytes[i] >> j);
            }
        }
        std::cout << "Cluster bitmap: ";
        for (auto &bit : bitmapCluster)
        {
            std::cout << (bit & 0b1);
        }
        std::cout << std::endl;

        // load save print inodes
        inodes.resize(sb.inodeCount);
        fileSystemImage.seekg(sb.inodeStart, std::ios::beg);
        std::cout << "Inodes: " << std::endl;
        for (int i = 0; i < sb.inodeCount; i++)
        {
            PseudoInode inode;
            fileSystemImage.read(reinterpret_cast<char *>(&inode), sizeof(PseudoInode));
            if ((bitmapInode[i] & 0b1) == 1)
            {
                inodes[i] = inode;
                std::cout << "Id: " << inode.nodeId << ", references: " << inode.references << ", " << ((inode.isDirectory == 1) ? "Directory" : "File")
                          << ", size: " << inode.fileSize << std::endl;
            }
        }
    }
    else
    {
        std::cout << "Please format filesystem image before use." << std::endl;
    }

    //while loop waiting and handeling user input
    std::cout << promptText;
    while (std::getline(std::cin, command))
    {
        int result = handleInput(command);
        if (result > 0)
        {
            //exit program
            break;
        }
        else
        {
            std::cout << promptText;
        }
    }

    return 0;
}