#pragma once

#include <iostream>
#include "constants.h"

#pragma pack(push, 1)

struct Superblock
{
    char signature[SIGNATURE_LENGTH];
    char volumeDescriptor[VOLUME_DESCRIPTOR_LENGTH];
    size_t diskSize;
    size_t inodeCount;
    size_t clusterSize;
    size_t clusterCount;
    size_t bitmapInodesStart;
    size_t bitmapClustersStart;
    size_t inodeStart;
    size_t clustersStartAddress;
};

struct PseudoInode
{
    int nodeId;
    bool isDirectory;
    int references;
    size_t fileSize;
    size_t direct1;
    size_t direct2;
    size_t direct3;
    size_t direct4;
    size_t direct5;
    size_t indirect1;
    size_t indirect2;
};

struct DirectoryItem
{
    int inode;
    char itemName[ITEM_NAME_LENGTH];
};

#pragma pack(pop)