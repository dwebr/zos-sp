#include "utils.h"

/**
 * returns array of strings from original string separated by separator
 **/
std::vector<std::string> explode(std::string &str, char separator)
{
    std::istringstream iss(str);
    std::vector<std::string> ret;

    std::string tmp;
    while (std::getline(iss, tmp, separator))
    {
        ret.push_back(tmp);
    }

    return ret;
}

/**
 * returns if string ends with ending
 **/
bool endsWith(std::string const &value, std::string const &ending)
{
    if (ending.size() > value.size())
        return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

/**
 * returns numer of bytes
 **/
size_t getBytes(std::string &size)
{
    size_t multiplicator = 1;
    int unitLength = 2;

    if ((endsWith(size, "KB")))
    {
        multiplicator = 1024l;
    }
    else if ((endsWith(size, "MB")))
    {
        multiplicator = 1024 * 1024l;
    }
    else if ((endsWith(size, "GB")))
    {
        multiplicator = 1024 * 1024 * 1024l;
    }
    else if (endsWith(size, "B"))
    {
        unitLength = 1;
    }
    else
    {
        return 0;
    }
    return (size_t)(std::stod(size.substr(0, size.length() - unitLength)) * multiplicator);
}

/**
 * prints superblock
 **/
void printSuperblock(Superblock &sb)
{
    std::cout << std::endl
              << "Signature: " << sb.signature << std::endl;
    std::cout << "Volume descriptor: " << sb.volumeDescriptor << std::endl
              << std::endl;
    std::cout << "Cluster size: " << sb.clusterSize << "B" << std::endl
              << std::endl;
    std::cout << "Inode count: " << sb.inodeCount << std::endl;
    std::cout << "Cluster count: " << sb.clusterCount << std::endl
              << std::endl;
    std::cout << "Start of inodeBitmap: " << sb.bitmapInodesStart << std::endl;
    std::cout << "Start of clusterBitmap: " << sb.bitmapClustersStart << std::endl;
    std::cout << "Start of inodes: " << sb.inodeStart << std::endl;
    std::cout << "Start of clusters: " << sb.clustersStartAddress << std::endl
              << std::endl;
    std::cout << "Space for data: " << sb.clusterCount * sb.clusterSize << "B" << std::endl;
}