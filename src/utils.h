#pragma once

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include "structs.h"

std::vector<std::string> explode(std::string &str, char separator = ' ');
size_t getBytes(std::string &size);
void printSuperblock(Superblock &sb);