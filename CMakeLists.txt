cmake_minimum_required(VERSION 3.0.0)
project(FSSimilator VERSION 0.1.0)

file(GLOB source_files
            "src/*.h"
            "src/*.cpp"
    )

add_executable(FSSimilator ${source_files})

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
